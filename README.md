**lux** is a very simple flashlight but it works from pulley menu when your *Jolla* is locked


### RPM

To create rpm: *rpmbuild --build-in-place -bb rpm/harbour-lux.spec*

otherwise, download it from jolla store or from https://openrepos.net/content/yuri/lux


### Note

After installation, you have to add **Lux** to pulley menu: go to **Settings**, press **System** then **Lock Screen**.
Now press **Add shortcut** and then **Lux**. Now you can turn on/off light from pulley menu even if your jolla phone is locked.
