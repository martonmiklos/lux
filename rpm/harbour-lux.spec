# Prevent brp-python-bytecompile from running.
%define __os_install_post %{___build_post}

Name: harbour-lux
Version: 1.4
Release: 1
Summary: A simple flashlight for Jolla phone
License: GPLv3+
URL: https://gitlab.com/yurib/lux.git
Source: %{name}-%{version}.tar.xz
BuildArch: noarch
BuildRequires: make

%description
This is a very simple flashlight but it works from pulley menu when your Jolla is locked

%prep
%setup -q

%post
udevadm control --reload-rules && udevadm trigger

%postun
udevadm control --reload-rules && udevadm trigger

%install
rm -rf %{buildroot}

# Desktop Entry
TARGET=%{buildroot}/%{_datadir}/applications
mkdir -p $TARGET
cp -rpv data/%{name}.desktop $TARGET/

# Icon
TARGET=%{buildroot}/%{_datadir}/icons/hicolor/86x86/apps/
mkdir -p $TARGET
cp -rpv data/%{name}.png $TARGET/%{name}.png

# bin
TARGET=%{buildroot}/usr/bin
mkdir -p $TARGET
cp -rpv data/%{name} $TARGET/

# udev rule
TARGET=%{buildroot}/etc/udev/rules.d
mkdir -p $TARGET
cp -rpv data/60-persistent-leds.rules $TARGET/
chmod 644 $TARGET/60-persistent-leds.rules

%files
%{_bindir}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
/etc/udev/rules.d/60-persistent-leds.rules
